package vfs.pg03.fred.cameraapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.SimpleOnScaleGestureListener;
import android.view.ViewGroup.LayoutParams;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

public class CameraSurface
    extends     SurfaceView
    implements  SurfaceHolder.Callback,
                Camera.AutoFocusCallback,
                Camera.FaceDetectionListener
{
    private Activity _context;
    private Camera _camera;
    private SurfaceHolder _surfaceHolder;
    private CameraSurfaceCallback _callback;
    private ScaleGestureDetector _scaleGestureDetector;
    private boolean _cameraInitialized;
    private FaceOverlayView _faceOverlayView;

    // The next variables are for controlling the zoom.
    private float _currentZoomFloat;
    private int _currentZoom;
    private boolean _zoomSupported;
    private int _maxZoom;
    private float _zoomMultiplier;

    // Lists to keep the supported camera features.
    public List<String> supportedColorEffects;
    public List<String> supportedFlashModes;
    public List<String> supportedSceneModes;
    public List<String> supportedWhiteBalance;

    public CameraSurface(Context context, CameraSurfaceCallback callback)
    {
        super(context);

        _context = (Activity) context;
        _surfaceHolder = getHolder();
        _surfaceHolder.addCallback(this);
        _callback = callback;
        _currentZoom = 0;
        _currentZoomFloat = 0;
        _cameraInitialized = false;
        _zoomMultiplier = 50f;

        // Init ScaleGestureDetector and forward zoom event to onZoomChange method.
        _scaleGestureDetector = new ScaleGestureDetector(_context, new SimpleOnScaleGestureListener() {
            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                onZoomChange(detector.getScaleFactor());
                return true;
            }
        });
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder)
    {
        try
        {
            _camera = Camera.open();
            _camera.setPreviewDisplay(surfaceHolder);

            // Start preview and face recognition
            restartCameraPreview();

            // Create the OverlayView
            _faceOverlayView = new FaceOverlayView(_context);
            _context.addContentView(_faceOverlayView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));

            // Set init flag
            _cameraInitialized = true;

            // Get supported features.
            Camera.Parameters camParams = _camera.getParameters();
            supportedColorEffects = camParams.getSupportedColorEffects();
            supportedFlashModes = camParams.getSupportedFlashModes();
            supportedSceneModes = camParams.getSupportedSceneModes();
            supportedWhiteBalance = camParams.getSupportedWhiteBalance();
            _zoomSupported = camParams.isZoomSupported();
            _maxZoom = camParams.getMaxZoom();

            if (_callback != null)
            {
                _callback.onCameraReady();
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int format, int width, int height)
    {
        try
        {
            // Get the current phone rotation.
            WindowManager winManager = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
            int phoneRotation = winManager.getDefaultDisplay().getRotation();
            int orientation = 0;

            switch (phoneRotation)
            {
                // Note that the camera is, by default, in landscape, so the angles
                // applied to the camera differs from the phone rotation.

                case Surface.ROTATION_0:
                    orientation = 90;
                    break;

                case Surface.ROTATION_270:
                    orientation = 180;
                    break;
            }

            // Set the rotation of the preview.
            _camera.setDisplayOrientation(orientation);

            // Set the rotation of the captured image.
            setRotation(orientation);

            // Set the face recognition orientation.
            if (_faceOverlayView != null)
            {
                _faceOverlayView.setDisplayOrientation(orientation);
            }

            // Apply changes to the preview.
            restartCameraPreview();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder)
    {
        _camera.stopPreview();
        _camera.release();
        _camera = null;
    }

    @Override
    public void onAutoFocus(boolean success, Camera camera)
    {
        if (success)
        {
            camera.cancelAutoFocus();
        }
    }

    @Override
    public void onFaceDetection(Camera.Face[] faces, Camera camera)
    {
        _faceOverlayView.setFaces(faces);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // Pass the captured data to ScaleGestureDetector
        _scaleGestureDetector.onTouchEvent(event);

        // Detect tap focus.
        detectFocusChange(event);

        return true;
    }

    private void detectFocusChange(MotionEvent event)
    {
        if(event.getAction() == MotionEvent.ACTION_DOWN)
        {
            float x = event.getX();
            float y = event.getY();

            Rect touchRect =
                new Rect(
                    (int)(x - 100),
                    (int)(y - 100),
                    (int)(x + 100),
                    (int)(y + 100)
                );

            final Rect targetFocusRect = new Rect(
                touchRect.left * 2000 / this.getWidth() - 1000,
                touchRect.top * 2000 / this.getHeight() - 1000,
                touchRect.right * 2000 / this.getWidth() - 1000,
                touchRect.bottom * 2000 / this.getHeight() - 1000
            );

            try
            {
                List<Camera.Area> focusList = new ArrayList<Camera.Area>();
                Camera.Area focusArea = new Camera.Area(targetFocusRect, 1000);
                focusList.add(focusArea);

                Camera.Parameters param = _camera.getParameters();
                param.setFocusAreas(focusList);
                param.setMeteringAreas(focusList);

                _camera.setParameters(param);
                _camera.autoFocus(this);

                if (_callback != null)
                {
                    _callback.onFocusChange((int)x, (int)y);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    // Handle changes in the zoom level.
    private void onZoomChange(float scaleFactor)
    {
        if (_cameraInitialized && _zoomSupported)
        {
            // Process the new zoom level in float.
            _currentZoomFloat += (scaleFactor - 1) * _zoomMultiplier;

            // Clamp the new value between 0 and the max supported zoom.
            if (_currentZoomFloat > _maxZoom)
            {
                _currentZoomFloat = _maxZoom;
            }
            else if (_currentZoomFloat < 0)
            {
                _currentZoomFloat = 0;
            }

            // The Camera API expect an int as the zoom level, so we round the float here.
            int roundedCurrentZoomFloat = Math.round(_currentZoomFloat);

            // If the rounded value has changed, update the camera.
            if (_currentZoom != roundedCurrentZoomFloat)
            {
                // Update the internal zoom value reference.
                _currentZoom = roundedCurrentZoomFloat;

                // Update the camera
                Camera.Parameters camParams = _camera.getParameters();
                camParams.setZoom(_currentZoom);
                _camera.setParameters(camParams);

                // Broadcast the change.
                if (_callback != null)
                {
                    _callback.onZoomChanged(_currentZoom);
                }
            }
        }
    }

    public void autoFocusThenTakePicture()
    {
        _camera.autoFocus(new Camera.AutoFocusCallback()
        {
            @Override
            public void onAutoFocus(boolean success, Camera camera)
            {
                takeMyPicture();
            }
        });
    }

    public void takeMyPicture()
    {
        _camera.takePicture(
            null,
            null,
            new Camera.PictureCallback()
            {
                @Override
                public void onPictureTaken(byte[] bytes, Camera camera)
                {
                    onJpegPictureTaken(bytes);
                }
            }
        );
    }

    public void onJpegPictureTaken(byte[] bytes)
    {
        try
        {
            File f = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            fo.write(bytes);
            fo.close();

            Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("image/jpeg");
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(f));
            _context.startActivity(Intent.createChooser(shareIntent, "Share Image"));

            restartCameraPreview();
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    private void restartCameraPreview()
    {
        _camera.startPreview();

        // start face detection only *after* preview has started
        restartFaceRecognition();
    }

    private void restartFaceRecognition()
    {
        _camera.setFaceDetectionListener(this); // setup listener

        Camera.Parameters camParams = _camera.getParameters();
        if (camParams.getMaxNumDetectedFaces() > 0) // detect feature support
        {
            // stop feature if it was running before.
            _camera.stopFaceDetection();

            // start feature
            _camera.startFaceDetection();
        }
    }

    private void setRotation(int rotation)
    {
        Camera.Parameters camParams = _camera.getParameters();
        camParams.setRotation(rotation);
        _camera.setParameters(camParams);
    }

    public void setFlashMode(String mode)
    {
        Camera.Parameters camParams = _camera.getParameters();
        camParams.setFlashMode(mode);
        _camera.setParameters(camParams);
    }

    public void setWhiteBalance(String mode)
    {
        Camera.Parameters camParams = _camera.getParameters();
        camParams.setWhiteBalance(mode);
        _camera.setParameters(camParams);
    }

    public void setColorEffect(String mode)
    {
        Camera.Parameters camParams = _camera.getParameters();
        camParams.setColorEffect(mode);
        _camera.setParameters(camParams);
    }

    public void setSceneMode(String mode)
    {
        Camera.Parameters camParams = _camera.getParameters();
        camParams.setSceneMode(mode);
        _camera.setParameters(camParams);
    }
}
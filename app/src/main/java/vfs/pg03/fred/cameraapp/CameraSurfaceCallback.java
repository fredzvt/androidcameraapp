package vfs.pg03.fred.cameraapp;

public interface CameraSurfaceCallback
{
    void onCameraReady();
    void onZoomChanged(int newZoomLevel);
    void onFocusChange(int x, int y);
}

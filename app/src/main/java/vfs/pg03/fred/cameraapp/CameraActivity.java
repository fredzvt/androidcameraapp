package vfs.pg03.fred.cameraapp;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.*;

public class CameraActivity extends Activity implements CameraSurfaceCallback
{
    // References to interface widgets.
    private FrameLayout _cameraLayout;
    private CameraSurface _cameraSurface;
    private TextView _textViewSettings;
    private Menu _menu;
    private Integer _currentSelectedSubMenuId;

    // Dynamic view ids for the menu options.
    private int _colorEffectsSubMenuId;
    private int _flashModesSubMenuId;
    private int _sceneModesSubMenuId;
    private int _whiteBalanceSubMenuId;

    // Keep the name of the currently selected feature to display on screen.
    private String _currentColorEffect;
    private String _currentFlashMode;
    private String _currentSceneMode;
    private String _currentWhiteBalanceMode;
    private String _currentZoom;
    private String _currentFocus;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        // Get a reference to layout items.
        getInterfaceWidgets();

        // Initialize settings display with their default values.
        _currentColorEffect = "none";
        _currentFlashMode = "auto";
        _currentSceneMode = "auto";
        _currentWhiteBalanceMode = "auto";
        _currentZoom = "0";
        _currentFocus = "auto";
        updateSettingsDisplay();

        // The setupCamera method is being called on onCreateOptionsMenu.
    }

    private void updateSettingsDisplay()
    {
        String endl = System.getProperty("line.separator");

        String text =
            "Effect: " + _currentColorEffect + endl +
            "Flash Mode: " + _currentFlashMode + endl +
            "Scene Mode: " + _currentSceneMode + endl +
            "White Balance Mode: " + _currentWhiteBalanceMode + endl +
            "Focus: " + _currentFocus + endl +
            "Zoom: " + _currentZoom;

        _textViewSettings.setText(text);
    }

    private void getInterfaceWidgets()
    {
        _cameraLayout = (FrameLayout) findViewById(R.id.camera_preview);
        _textViewSettings = (TextView) findViewById(R.id.textViewSettings);
    }

    private void setupCamera()
    {
        _cameraSurface = new CameraSurface(this, this);
        _cameraLayout.addView(_cameraSurface);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        _menu = menu;
        menu.add("Initializing...");

        // To make sure that the menu will be available after the camera
        // initialization I'm calling the setupCamera method here.
        setupCamera();

        return true;
    }

    @Override
    public void onCameraReady()
    {
        generateViewIdsForCameraSupportedFeaturesMenuItems();
        createMenuBasedOnCameraFeatures();
    }

    @Override
    public void onZoomChanged(int newZoomLevel)
    {
        _currentZoom = String.valueOf(newZoomLevel);
        updateSettingsDisplay();
    }

    @Override
    public void onFocusChange(int x, int y)
    {
        _currentFocus = "(" + x + "," + y + ")";
        updateSettingsDisplay();
    }

    private void generateViewIdsForCameraSupportedFeaturesMenuItems()
    {
        // Generate the unique ids for the submenu items.
        _colorEffectsSubMenuId = View.generateViewId();
        _flashModesSubMenuId = View.generateViewId();
        _sceneModesSubMenuId = View.generateViewId();
        _whiteBalanceSubMenuId = View.generateViewId();
    }

    private void createMenuBasedOnCameraFeatures()
    {
        // Clear the temporary "Initializing..." item.
        _menu.clear();

        // Fore each top menu, add supported features.
        createMenuBasedOnCameraFeatures(_cameraSurface.supportedColorEffects, _colorEffectsSubMenuId, "Effects");
        createMenuBasedOnCameraFeatures(_cameraSurface.supportedFlashModes, _flashModesSubMenuId, "Flash Modes");
        createMenuBasedOnCameraFeatures(_cameraSurface.supportedSceneModes, _sceneModesSubMenuId, "Scene Modes");
        createMenuBasedOnCameraFeatures(_cameraSurface.supportedWhiteBalance, _whiteBalanceSubMenuId, "White Balance");
    }

    private void createMenuBasedOnCameraFeatures(List<String> features, int subMenuId, String subMenuTitle)
    {
        SubMenu subMenuItem = _menu.addSubMenu(Menu.NONE, subMenuId, Menu.NONE, subMenuTitle);

        if (features == null || features.isEmpty())
        {
            subMenuItem.add("None supported.");
        }
        else
        {
            for (String feature : features)
            {
                subMenuItem.add(feature);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int menuId = item.getItemId();

        if (menuId == _colorEffectsSubMenuId ||
            menuId == _flashModesSubMenuId ||
            menuId == _sceneModesSubMenuId ||
            menuId == _whiteBalanceSubMenuId)
        {
            // _currentSelectedSubMenuId keeps track of which
            // submenu was last selected.

            _currentSelectedSubMenuId = menuId;
        }
        else
        {
            // If the selected item is not a submenu, it is a feature item.
            // Now, based on the last selected submenu, we know which feature
            // is being changed.

            String feature = item.getTitle().toString();

            if (_currentSelectedSubMenuId.equals(_colorEffectsSubMenuId))
            {
                _cameraSurface.setColorEffect(feature);
                _currentColorEffect = feature;
            }
            else if (_currentSelectedSubMenuId.equals(_flashModesSubMenuId))
            {
                _cameraSurface.setFlashMode(feature);
                _currentFlashMode = feature;
            }
            else if (_currentSelectedSubMenuId.equals(_sceneModesSubMenuId))
            {
                _cameraSurface.setSceneMode(feature);
                _currentSceneMode = feature;
            }
            else if (_currentSelectedSubMenuId.equals(_whiteBalanceSubMenuId))
            {
                _cameraSurface.setWhiteBalance(feature);
                _currentWhiteBalanceMode = feature;
            }

            updateSettingsDisplay();
        }

        return super.onOptionsItemSelected(item);
    }

    public void takePictureClick(View view)
    {
        _cameraSurface.autoFocusThenTakePicture();
    }
}
